<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="../style/style.css">
    </head>

    <body>

<?php 
// require('../includes/config.php');

try
{
	$db = new PDO('mysql:host=localhost;dbname=blogfromscratch;charset=utf8', 'root' , 'password');
}
catch(Exception $e)
{
        die('Erreur : '.$e->getMessage());
}

$stmt = $db->query( 
'SELECT * FROM articles
JOIN authors ON author_id = authors.id
JOIN categories ON category = categories.category'
);

while ($articles = $stmt->fetch()){
?>


<div class = "affichage">


<div id ="images">
       <img src=<?= $articles['image_url'];?> > <br/>
</div>
<div id="informations">
        Titre : <em> <?= $articles['title'];?> </em> <br/>
        Author : <strong> <?= $articles['firstname'] . ' ' . $articles['lastname'];?> </strong> <br/>
        Reading time estimated: <?= $articles['reading_time']?> minutes.<br/>
        Category : <em><?= $articles['category'];?></em> <br/>
        Content : <?= strip_tags(substr($articles['content'], 0, 300));?> <br><br>
</div>
<?php

$categories = $db->query( 
'SELECT * FROM articles_categories
JOIN categories ON category = categories.category'
        );
;
?>


</div>
<?php
}
?>
</body>
</html>