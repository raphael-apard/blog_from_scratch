<form action='' method='post'>

    <p><label>Username</label><br />
    <input type='text' name='username' value='<?php if(isset($error)){ echo $_POST['username'];}?>'></p>

    <p><label>Password</label><br />
    <input type='password' name='password' value='<?php if(isset($error)){ echo $_POST['password'];}?>'></p>

    <p><label>Confirm Password</label><br />
    <input type='password' name='passwordConfirm' value='<?php if(isset($error)){ echo $_POST['passwordConfirm'];}?>'></p>

    <p><label>Email</label><br />
    <input type='text' name='email' value='<?php if(isset($error)){ echo $_POST['email'];}?>'></p>
    
    <p><input type='submit' name='submit' value='Add User'></p>

</form>

<?php

$hashedpassword = $user->create_hash($password);

try {

    //ajout dans la base de données
    $stmt = $db->prepare(
        'INSERT INTO authors (firstname,lastname,password,email) 
        VALUES (:username, :password, :email)') ;
    $stmt->execute(array(
        ':firstname' => $firstname,
        ':lastname' =>$lastname,
        ':password' => $hashedpassword,
        ':email' => $email
    ));

    //redirige sur l'index
    header('Location: users.php?action=added');
    exit;

} catch(PDOException $e) {
    echo $e->getMessage();
}