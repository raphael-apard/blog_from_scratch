test de la page index
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="../style/style.css">
        <title>Index_Public</title>
    </head>

    <body>
        <h1>Index principal</h1> 

<table>
<tr>
    <th>Title</th>
    <th>Date</th>
    <th>Content</th>
</tr>

<?php 
//require_once('../includes/config.php');
    try {

        $stmt = $db->query( 
            'SELECT * FROM articles
            JOIN authors ON author_id = authors.id
            JOIN categories ON category = categories.category'
            );
            
            while ($articles = $stmt->fetch()){
            
            echo '<tr>';
            echo '<td>'.$row['title'].'</td>';
            echo '<td>'.date('jS M Y', strtotime($row['published_at'])).'</td>';
            ?>
            <td>
                <a href="edit-post.php?id=<?php echo $row['id'];?>">Edit</a> | 
                <a href="javascript:delpost('<?php echo $row['id'];?>','<?php echo $row['title'];?>')">Delete</a>
            </td>
            <!--Le javascript sert à afficher un message de vérification avant de supprimer un billet de blog-->
            
            <?php 
            echo '</tr>';

        }

    } catch(PDOException $e) {
        echo $e->getMessage();
    }
?>


<?php
if(isset($_GET['delpost'])){ 

$stmt = $db->prepare('DELETE FROM articles WHERE id = :id') ;
$stmt->execute(array(':id' => $_GET['delpost']));

header('Location: index.php?action=deleted');
exit;
} 
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Admin</title>
  <link rel="stylesheet" href="../style/normalize.css">
  <link rel="stylesheet" href="../style/main.css">
  <script language="JavaScript" type="text/javascript">
  function delpost(id, title)
  {
      if (confirm("Are you sure you want to delete '" + title + "'"))
      {
          window.location.href = 'index.php?delpost=' + id;
      }
  }
  </script>


</head>
<body>

    <div id="container">

    <?php include('menu.php');?>

    <?php 
    //show message from add / edit page
    if(isset($_GET['action'])){ 
        echo '<h3>Post '.$_GET['action'].'.</h3>'; 
    } 
    ?>

    <table>
    <tr>
        <th>Title</th>
        <th>Date</th>
        <th>Action</th>
    </tr>
    <?php
        try {

            $stmt = $db->query('SELECT id, title, published_at FROM articles ORDER BY id DESC');
            while($row = $stmt->fetch()){
                
                echo '<tr>';
                echo '<td>'.$row['title'].'</td>';
                echo '<td>'.date('jS M Y', strtotime($row['published_at'])).'</td>';
                ?>

                <td>
                    <a href="edit-post.php?id=<?php echo $row['article.id'];?>">Edit</a> | 
                    <a href="javascript:delpost('<?php echo $row['article.id'];?>','<?php echo $row['aticle.title'];?>')">Delete</a>
                </td>
                
                <?php 
                echo '</tr>';

            }

        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    ?>
    </table>

    <p><a href='add-post.php'>Add Post</a></p>

</div>

<script language="JavaScript" type="text/javascript">
function delpost(id, title)
{
  if (confirm("Are you sure you want to delete '" + title + "'"))
  {
      window.location.href = 'index.php?delpost=' + id;
  }
}
</script>
</body>
</html>

