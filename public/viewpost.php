Test de la page ViewPost
<?php 
require_once('../includes/config.php');

//affiche les articles cliqués
$stmt = $db->prepare(
    'SELECT * FROM articles
    JOIN authors ON author_id = authors.id
    JOIN categories ON category = categories.category'
);

//si pas de post : GO TO INDEX
if($row['article.id'] == ''){
    header('Location: /public/index.php');
    exit;
}

//affiche l'ensemble de l'article
echo '<div>';
    echo '<h1>'.$row['article.title'].'</h1>';
    echo '<p>Posted on '.date('jS M Y', strtotime($row['postDate'])).'</p>';
    echo '<p>'.$row['postCont'].'</p>';                
echo '</div>';
?>